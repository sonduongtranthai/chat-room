# Getting Started with Chat-room Backend App

This is a simple chat-room project ursing Nodejs, ExpressJs, Mongoose and Socket.io

# Setup
## ENVIRONMENT

Before start project, create environment file name `.env` and add values, for example:
`.env.example` with this content:
```text
MONGO_URL=mongodb://localhost:27017/chat
PORT=8000
```

### `npm install`

Downloads all packages and it's dependencies.

### `npm run dev`
Runs the app in the development mode.
This backend will open on [http://localhost:8000](http://localhost:8000) by default.

### `npm start`

Runs the app in the production mode.