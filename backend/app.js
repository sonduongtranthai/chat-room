const createError = require('http-errors')
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const dotenv = require('dotenv')
const mongoose = require('mongoose')
const cors = require('cors')
const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const roomsRouter = require('./routes/rooms')
const messagesRouter = require('./routes/messages')
const http = require('http')
const socket = require('socket.io')
dotenv.config()

mongoose.connect(
  process.env.MONGO_URL,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => {
    console.log('Connected to MongoDB')
  }
)
const app = express()
const server = http.createServer(app)

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/api/users', usersRouter)
app.use('/api/rooms', roomsRouter)
app.use('/api/messages', messagesRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}
  // render the error page
  res.status(err.status || 500)
  res.render('error')
})
const port = process.env.PORT || 8000
server.listen(port, function () {
  console.log('Server listen on port', port)
})

// Socket.io
let users = []

const addUser = (userId, socketId, roomId) => {
  !users.some((user) => user.userId === userId && user.roomId === roomId) &&
    users.push({ userId, socketId, roomId })
}

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId)
}

const getUsers = (roomId, userId) => {
  return users.filter((user) => user.roomId === roomId && user.userId !== userId)
}
const io = socket(server, {
  cors: {
    origin: '*'
  }
})
io.on('connection', (socket) => {
  console.log('new user connected!')
  socket.on('addUser', ({ userId, roomId }) => {
    addUser(userId, socket.id, roomId)
  })

  // send and get message
  socket.on('sendMessage', (payload) => {
    const users = getUsers(payload.roomId, payload.userId)
    users.forEach(user => {
      io.to(user.socketId).emit('getMessage', payload)
      console.log(user.userId, payload)
    })
  })

  // when disconnect
  socket.on('disconnect', () => {
    console.log('a user disconnected!')
    removeUser(socket.id)
    io.emit('getUsers', users)
  })
})

module.exports = app
