const mongoose = require('mongoose')

const RoomSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    },
    members: {
      type: Array
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Room', RoomSchema)
