const express = require('express')
const Message = require('../models/Message')
const User = require('../models/User')
const router = express.Router()

/* GET messages by roomId */
router.get('/:roomId', async (req, res, next) => {
  try {
    const messages = await Message.find({
      roomId: req.params.roomId
    }).populate('sender')
    res.status(200).json(messages)
  } catch (err) {
    res.status(500).json(err)
  }
})

/* POST add message */

router.post('/', async (req, res) => {
  const message = new Message(req.body)
  try {
    const saveMessage = await message.save()
    const sender = await User.findById(saveMessage.sender)
    const result = {
      text: saveMessage.text,
      roomId: saveMessage.roomId,
      sender
    }
    res.status(200).json(result)
  } catch (err) {
    res.status(500).json(err)
  }
})

module.exports = router
