const express = require('express')
const Room = require('../models/Room')
const User = require('../models/User')
const router = express.Router()

/* GET room */

router.get('/:id', async (req, res) => {
  try {
    const room = await Room.findById(req.params.id)
    res.status(200).json(room)
  } catch (err) {
    res.status(500).json(err)
  }
})

/* POST add new room */

router.post('/', async (req, res) => {
  const room = new Room(req.body)
  try {
    const saveRoom = await room.save()
    res.status(200).json(saveRoom)
  } catch (err) {
    res.status(500).json(err)
  }
})

/* POST join room */

router.post('/join', async (req, res) => {
  try {
    const { roomName, username } = req.body
    const roomFind = await Room.findOne({ name: roomName })
    let user = await User.findOne({ username })
    if (!user) user = await User.create({ username })
    if (roomFind) {
      if (roomFind.members.indexOf(user._id.toString()) > -1) { return res.status(400).json({ message: 'This username already in room' }) } else {
        roomFind.members.push(user._id.toString())
        const saveRoom = await roomFind.save()
        res.status(200).json({
          roomId: saveRoom._id,
          roomName: saveRoom.name,
          userId: user._id,
          username: user.username
        })
      }
    }
    if (!roomFind) {
      const room = new Room({ name: roomName, members: [user._id.toString()] })
      const saveRoom = await room.save()
      res.status(200).json({
        roomId: saveRoom._id,
        roomName: saveRoom.name,
        userId: user._id,
        username: user.username
      })
    }
  } catch (err) {
    console.log(err)
    res.status(500).json(err)
  }
})

/* POST exit room */

router.post('/exit', async (req, res) => {
  try {
    const { roomId, userId } = req.body
    const roomFind = await Room.findById(roomId)
    if (!roomFind) {
      return res.status(400).json({ message: 'This room is not exited!' })
    }
    if (roomFind.members.indexOf(userId) > -1) {
      roomFind.members = roomFind.members.filter(item => item !== userId)
      const saveRoom = await roomFind.save()
      return res.status(200).json(saveRoom)
    } else {
      res.status(400).json({ message: 'This username is not join this room!' })
    }
  } catch (err) {
    res.status(500).json(err)
  }
})

module.exports = router
