const express = require('express')
const User = require('../models/User')
const router = express.Router()

/* GET user by username */
router.get('/:username', async (req, res, next) => {
  try {
    const user = await User.findOne({
      username: req.params.username
    })
    res.status(200).json(user)
  } catch (err) {
    res.status(500).json(err)
  }
})

/* POST add user */

router.post('/', async (req, res) => {
  const user = new User(req.body)
  try {
    const saveUser = await user.save()
    res.status(200).json(saveUser)
  } catch (err) {
    res.status(500).json(err)
  }
})

/* GET or ADD user by username */
router.post('/check', async (req, res, next) => {
  const { username } = req.body
  try {
    const user = await User.findOne({
      username
    })
    if (user) { return res.status(200).json(user) }
    const newUser = new User({ username })
    const saveUser = await newUser.save()
    return res.status(200).json(saveUser)
  } catch (err) {
    res.status(500).json(err)
  }
})

module.exports = router
