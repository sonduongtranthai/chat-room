# Getting Started with Chat-room Frontend App

This is a simple chat-room project usesing ReactJs

# Setup
## ENVIRONMENT

Before start project, create environment file name `.env` and add values, for example:
`.env.example` with this content:
```text
REACT_APP_API_URL=http://localhost:8000/api
REACT_APP_SOCKET_URL=ws://localhost:8000
```

### `npm install`

Downloads all packages and it's dependencies.

### `npm start`
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode. But now, I have not developed the tests yet.
There will be development orientation for the next version.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!
