import './App.css'
import Login from './pages/login/Login'
import Messenger from './pages/messenger/Messenger'
import { UserProvider } from './context/UserContext'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

function App () {
  return (
    <UserProvider>
      <Router>
        <Switch>
          <Route path='/' exact><Login /></Route>
          <Route path='/room/:id' exact><Messenger /></Route>
        </Switch>
      </Router>
    </UserProvider>

  )
}

export default App
