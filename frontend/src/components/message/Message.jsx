import React from 'react'
import './message.css'

function Message (props) {
  const { message, own } = props
  return (
    <div className={own ? 'message_box own' : 'message_box not_own'}>
      {!own &&
        <div className='message_sender'>
          {message.sender.username}
        </div>}
      <div className='message_text last'>
        {message.text}
      </div>
    </div>
  )
}

export default Message
