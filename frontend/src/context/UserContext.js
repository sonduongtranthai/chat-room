import React, { useState } from 'react'

const UserContext = React.createContext({
  username: '',
  roomId: '',
  userId: '',
  roomName: '',
  setContext: () => {}
})
export const InitState = {
  username: '',
  roomId: '',
  userId: '',
  roomName: ''
}

export default UserContext

export const UserProvider = (props) => {
  const setContext = (context) => {
    setState({ ...state, ...context })
  }

  const initState = {
    username: '',
    roomId: '',
    userId: '',
    roomName: '',
    setContext: setContext
  }

  const [state, setState] = useState(initState)

  return (
    <UserContext.Provider value={state}>
      {props.children}
    </UserContext.Provider>
  )
}
