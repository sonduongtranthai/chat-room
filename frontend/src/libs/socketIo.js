import socketClient from 'socket.io-client'

const socketIo = socketClient(process.env.REACT_APP_SOCKET_URL || '/socket.io')

export default socketIo
