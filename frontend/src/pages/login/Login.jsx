import React, { useContext, useRef, useState, useEffect } from 'react'
import './login.css'
import request from '../../libs/request'
import { useHistory } from 'react-router-dom'
import UserContext, { InitState } from '../../context/UserContext'

function Login () {
  const username = useRef()
  const roomName = useRef()
  const [isFetching, setIsFetching] = useState(false)
  const [error, setError] = useState('')
  const history = useHistory()
  const context = useContext(UserContext)

  const handleSubmit = (e) => {
    e.preventDefault()
    setError('')
    setIsFetching(true)
    const payload = { username: username.current.value, roomName: roomName.current.value }
    request.post('/rooms/join', payload).then(res => {
      if (!res) { return setError('Can not create roomData') }
      localStorage.setItem('userId', res.userId)
      localStorage.setItem('username', res.username)
      context.setContext({ roomName: res.roomName, username: res.username, roomId: res.roomId, userId: res.userId })
    }).catch((e) => {
      setError(`(*) ${e.message}`)
    }).then(() => setIsFetching(false))
  }

  useEffect(() => {
    context.setContext(InitState)
  }, [])

  useEffect(() => {
    if (context.roomId && context.userId) { history.push(`/room/${context.roomId}`) }
  }, [context])

  return (
    <div className='login'>
      <div className='login_wrapper'>

        <h3 className='login_title'>
          Join Chatroom
        </h3>

        <form className='login_box' onSubmit={handleSubmit}>

          <input
            placeholder='Username'
            type='text'
            required
            minLength='3'
            className='login_input'
            ref={username}
          />

          <input
            placeholder='RoomId'
            type='text'
            required
            className='login_input'
            ref={roomName}
          />

          <button
            style={{ cursor: !isFetching ? 'pointer' : 'wait' }}
            className='login_button' type='submit' disabled={isFetching}
          >
            Join
          </button>

          {
            error && <p className='alert'> {error} </p>
          }
        </form>

      </div>
    </div>
  )
}

export default Login
