import React, { useEffect, useRef, useState, useContext } from 'react'
import './messenger.css'
import Message from '../../components/message/Message'
import request from '../../libs/request'
import UserContext from '../../context/UserContext'
import { useLocation, useHistory } from 'react-router-dom'
import socketIo from '../../libs/socketIo'

function Messenger () {
  const [messages, setMessages] = useState([])
  const [newMessage, setNewMessage] = useState('')
  const scrollRef = useRef()
  const context = useContext(UserContext)
  const { roomName, userId, roomId } = context
  const location = useLocation()
  const history = useHistory()
  const socket = useRef()
  const [messageCome, setMessageCome] = useState(null)
  const paramString = location.pathname
  const id = paramString.slice(paramString.indexOf('/', 1) + 1, paramString.length)
  useEffect(() => {
    socket.current = socketIo
    socket.current.on('getMessage', (newMessage) => {
      setMessageCome(newMessage)
    })
    if (!roomName) {
      request.get('/rooms/' + id).then(room => {
        if (room) context.setContext({ ...context, roomName: room.name, roomId: room._id })
      }).catch(e => console.log(e))
    }
  }, [])

  useEffect(() => {
    if (messageCome) {
      setMessages([...messages, messageCome])
      setMessageCome(null)
    }
  }, [messageCome])

  useEffect(() => {
    if (userId) {
      fetchMessage(id)
    } else {
      const userIdFromStorage = localStorage.getItem('userId')
      if (userIdFromStorage) {
        context.setContext({ ...context, userId: userIdFromStorage })
      } else history.push('/')
    }
  }, [userId])

  useEffect(() => {
    if (userId && roomId) { socket.current.emit('addUser', { userId, roomId }) }
  }, [userId, roomId])

  const fetchMessage = (id) => {
    request.get(`/messages/${id}`).then(res => {
      setMessages(res)
    })
  }

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: 'smooth' })
  }, [messages])

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      handleSubmit()
    }
  }

  const handleSubmit = () => {
    if (!newMessage) return
    const payload = {
      sender: userId,
      roomId: roomId,
      text: newMessage
    }
    request.post('/messages', payload).then(newMessageResponse => {
      const sendMessage = { ...newMessageResponse, userId: newMessageResponse.sender._id }
      setMessages([...messages, sendMessage])
      socket.current.emit('sendMessage', sendMessage)
      setNewMessage('')
    }).catch(e => console.log(e))
  }

  const exitRoom = () => {
    const payload = {
      userId: userId,
      roomId: roomId
    }
    request.post('/rooms/exit', payload).then(result => {
      localStorage.clear()
    }).catch(e => console.log(e))
      .then(() => history.push('/'))
  }

  return (
    <div className='messenger'>
      <div className='messenger_wrapper'>

        <div className='messenger_header'>
          <button
            className='messenger_exit'
            onClick={() => exitRoom()}
          >Exit
          </button>
          <h3 className='messenger_title'>
            {roomName || 'Join Chatroom'}
          </h3>
        </div>

        <div className='chat_box'>
          {messages.map(item => (
            <div ref={scrollRef} key={`${item._id}-${Math.random()}`}>
              <Message message={{ ...item }} own={userId === item.sender._id.toString()} />
            </div>
          ))}
        </div>

        <div className='message_input_container'>
          <input
            placeholder='Message here...'
            type='text'
            className='messenger_input'
            onKeyDown={handleKeyDown}
            onChange={(e) => setNewMessage(e.target.value)}
            value={newMessage}
          />
          <div
            onClick={() => {
              handleSubmit()
            }}
            className='message_input_icon'
          >
            <svg
              width='16' height='16' viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'
            >
              <path d='M6 1L6.70711 0.292893C6.31658 -0.0976312 5.68342 -0.0976312 5.29289 0.292893L6 1ZM10.2929 6.70711C10.6834 7.09763 11.3166 7.09763 11.7071 6.70711C12.0976 6.31658 12.0976 5.68342 11.7071 5.29289L10.2929 6.70711ZM0.292893 5.29289C-0.0976312 5.68342 -0.0976312 6.31658 0.292893 6.70711C0.683417 7.09763 1.31658 7.09763 1.70711 6.70711L0.292893 5.29289ZM5 15C5 15.5523 5.44772 16 6 16C6.55228 16 7 15.5523 7 15H5ZM5.29289 1.70711L10.2929 6.70711L11.7071 5.29289L6.70711 0.292893L5.29289 1.70711ZM5.29289 0.292893L0.292893 5.29289L1.70711 6.70711L6.70711 1.70711L5.29289 0.292893ZM5 1V8H7V1H5ZM5 8V15H7V8H5Z' fill='white' />
            </svg>
          </div>
        </div>

      </div>
    </div>
  )
}

export default Messenger
